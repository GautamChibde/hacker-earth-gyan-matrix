package com.gc.comman;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.gc.comman.infra.NetworkManager;
import com.orm.SugarContext;

import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    private static final String TAG = App.class.getSimpleName();
    private NetworkManager networkManager;
    private SharedPreferences sharedPref;
    private static String BASE_URL = "";
    private String credentials;
    @Override
    public void onCreate() {
        super.onCreate();
        if (null != getSharedPrefKey()) {
            sharedPref = getSharedPreferences(getSharedPrefKey(), MODE_PRIVATE);
        } else {
            throw new RuntimeException("Please specify Shared pref key");
        }
        networkManager = new NetworkManager(this);

    }

    // Shared Preferences
    public String getSharedPrefKey() {
        return "";
    }

    public void saveToPref(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveToPref(Map<String, Object> values) {
        SharedPreferences.Editor editor = sharedPref.edit();
        for (String key : values.keySet()) {
            if (values.get(key) instanceof String) {
                editor.putString(key, (String) values.get(key));
            } else if (values.get(key) instanceof Long) {
                editor.putLong(key, (Long) values.get(key));
            }
        }
        editor.apply();
    }

    public String getFromPref(String key) {
        return sharedPref.getString(key, null);
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }

    public String getBaseUri() {
        return "";
    }

    protected void init() {
        initLocalDb();
    }

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    protected void initLocalDb() {
        SugarContext.init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        try {
            SugarContext.terminate();
        } catch (Throwable t) {
            Log.i(TAG, t.toString());
        }
        super.onTerminate();
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public String getCredentials() {
        return credentials;
    }

    protected void setBaseUrl(String url) {
        BASE_URL = url;
    }

    protected String getBaseUrl() {
        return BASE_URL;
    }

    public NetworkManager getNetworkManager() {
        return networkManager;
    }
}
