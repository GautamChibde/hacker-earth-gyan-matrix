package com.gc.comman.infra;

import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Resource {

    public static <T> void enquire(Call<T> call,final int requestId){
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                EventBus.getDefault().post(
                        new HttpRequestComplete<>(
                                requestId,
                                true,
                                null,
                                response.body()));
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                EventBus.getDefault().post(
                        new HttpRequestComplete<T>(
                                requestId,
                                false,
                                t.getMessage(),
                                null));
            }
        });
    }
    public static <T> void enquireClone(Call<T> call,final int requestId){
        call.clone().enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                EventBus.getDefault().post(
                        new HttpRequestComplete<>(
                                requestId,
                                true,
                                null,
                                response.body()));
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                EventBus.getDefault().post(
                        new HttpRequestComplete<T>(
                                requestId,
                                false,
                                t.getMessage(),
                                null));
            }
        });
    }
    public static class HttpRequestComplete<T> {
        private final int requestId;
        private final boolean successful;
        private final String message;
        private final T response;

        public HttpRequestComplete(int requestId, boolean success,
                                   String message, T response) {
            this.requestId = requestId;
            this.successful = success;
            this.message = message;
            this.response = response;
        }

        @Override
        public String toString() {
            return "HttpRequestComplete{" +
                    "requestId=" + requestId +
                    ", response =" + response +
                    ", successful=" + successful +
                    ", message='" + message + '\'' +
                    '}';
        }

        public boolean isSuccessful() {
            return successful;
        }

        public String getMessage() {
            return message;
        }

        public int getRequestId() {
            return requestId;
        }

        public T getResponse() {
            return response;
        }
    }
}
