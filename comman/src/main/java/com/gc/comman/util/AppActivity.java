package com.gc.comman.util;


import com.gc.comman.App;

public interface AppActivity {

    App getApp();

    void askPermission(int requestId, String... permissions);

}
