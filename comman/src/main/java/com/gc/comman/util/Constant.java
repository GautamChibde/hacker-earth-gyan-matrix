package com.gc.comman.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Constant {
    String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    SimpleDateFormat timeFormatter =
            new SimpleDateFormat("HH:mm", Locale.getDefault());
    SimpleDateFormat defaultDateFormatter =
            new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    String MODEL = "MODEL";

    String KEY_USER_TOKEN = "KEY_USER_TOKEN";
    String KEY_CLIENT_TOKEN = "KEY_CLIENT_TOKEN";
    String KEY_USER_ROL = "KEY_USER_ROL";
    String KEY_USER_ID = "KEY_USER_ID";
    String FOR_RESULT = "FOR_RESULT";
    String REQUEST_CODE = "REQUEST_CODE";
    String GIT_AUTH = "git_auth";
    int AUTH_REQUEST_CODE = 102;
    int ACTIVITY_RESULT = 234;
    String RECORDS = "records";
}
