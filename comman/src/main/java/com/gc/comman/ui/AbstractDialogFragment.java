package com.gc.comman.ui;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

@SuppressWarnings("unused")
public class AbstractDialogFragment extends DialogFragment {
    private CharSequence title;
    private TextView textError;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getLayoutId() != 0) {
            View root = inflater.inflate(getLayoutId(), container, false);
            if (getErrorView() != 0) {
                textError = (TextView) root.findViewById(getErrorView());
                textError.setVisibility(View.GONE);
            }
            initView(root);
            return root;
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected int getLayoutId() {
        return 0;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
        }
    }

    protected void setErrorText(String message) {
        textError.setVisibility(View.VISIBLE);
        textError.setText(message);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (getDialogAnimation() != 0) {
            dialog.getWindow().getAttributes().windowAnimations = getDialogAnimation();
        }
        dialog.show();
        return dialog;
    }

    protected void setupToolbar(Toolbar toolbar, String title,
                                int backDrawable, int menu, int titleId,
                                Toolbar.OnMenuItemClickListener listener) {
        toolbar.setOnMenuItemClickListener(listener);
        if (menu != 0) {
            toolbar.inflateMenu(menu);
        }
        if (titleId != 0) {
            TextView tvTitle = (TextView) toolbar.findViewById(titleId);
            tvTitle.setText(title);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected int getDialogAnimation() {
        return 0;
    }

    protected void showDialog(DialogFragment fragment, Bundle dataBundle, String tag) {
        fragment.setArguments(dataBundle);
        fragment.show(getActivity().getFragmentManager(), tag);
    }

    public void onRequestPermissionsResult(int requestId,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }

    protected void initView(View v) {
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(getActivity(), permission);
    }

    protected int getErrorView() {
        return 0;
    }
}
