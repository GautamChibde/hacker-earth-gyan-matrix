package com.gc.comman.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gc.comman.model.Link;
import com.gc.comman.model.ViewFactory;

import java.util.List;

public class ItemListAdapter extends AbstractListAdapter<Link> {

    public ItemListAdapter(Context context, List<Link> links) {
        super(context, links);
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return ViewFactory.getDrawerItemView(getContext(), 18, 32);
    }

    @Override
    protected void initView(View view, int position) {
        if (view instanceof TextView) {
            initTextView((TextView) view, getItem(position));
        }
    }

    protected void initTextView(TextView drawerLink, Link link) {
        drawerLink.setText(link.getName());
        if (0 != link.getImageId()) {
            drawerLink.setCompoundDrawablesWithIntrinsicBounds(
                    link.getImageId(), 0, 0, 0);
        }
    }
}
