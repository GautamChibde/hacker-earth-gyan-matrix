package com.gc.comman.ui;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gc.comman.App;
import com.gc.comman.util.AppActivity;
import com.gc.comman.util.PlatformUtil;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment {
    private TextView textError;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof AppActivity)) {
            throw new RuntimeException("Abstract fragment can only be used " +
                    "with a host of type BaseActivity");
        }
        if (isEventListener()) {
            registerAsListener();
        }
        setHasOptionsMenu(getOptionsMenu() != 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (getLayoutId() != 0) {
            View root = inflater.inflate(getLayoutId(), container, false);
            if (!TextUtils.isEmpty(getTitle())) {
                getActivity().setTitle(getTitle());
            }
            if (getErrorView() != 0) {
                textError = (TextView) root.findViewById(getErrorView());
                textError.setVisibility(View.GONE);
            }
            initView(root);
            return root;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getOptionsMenu() != 0) {
            inflater.inflate(getOptionsMenu(), menu);
        }
    }

    protected void setErrorText(String message) {
        textError.setVisibility(View.VISIBLE);
        textError.setText(message);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    protected void registerAsListener() {
        EventBus.getDefault().register(this);
    }

    public App getApp() {
        return ((AppActivity) getActivity()).getApp();
    }

    protected void showDialog(DialogFragment fragment, Bundle dataBundle, String tag) {
        fragment.setArguments(dataBundle);
        fragment.show(getActivity().getFragmentManager(), tag);
    }

    protected void askPermission(int requestId, String... permissions) {
        List<String> needPerms = new ArrayList<>();
        int[] grantResults = new int[permissions.length];
        for (int i = 0; i < permissions.length; i++) {
            if (permissions[i] != null) {
                if (!hasPermission(permissions[i])) {
                    needPerms.add(permissions[i]);
                } else {
                    grantResults[i] = PackageManager.PERMISSION_GRANTED;
                }
            }
        }
        if (needPerms.size() > 0) {
            FragmentCompat.requestPermissions(this,
                    needPerms.toArray(new String[needPerms.size()]), requestId);
        } else {
            onRequestPermissionsResult(requestId, permissions, grantResults);
        }
    }

    public void onRequestPermissionsResult(int requestId,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(getActivity(), permission);
    }

    protected void showKeyboard(View view) {
        if (view != null) {
            view.requestFocus();
            PlatformUtil.showKeyboard(getActivity(), view);
        }
    }

    protected String getTitle() {
        return "";
    }

    protected int getOptionsMenu() {
        return 0;
    }

    protected int getLayoutId() {
        return 0;
    }

    protected void initView(View v) {
    }

    protected boolean isEventListener() {
        return false;
    }

    protected void post(Object event) {
        EventBus.getDefault().post(event);
    }

    protected int getErrorView() {
        return 0;
    }
}
