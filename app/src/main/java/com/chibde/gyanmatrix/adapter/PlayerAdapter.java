package com.chibde.gyanmatrix.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chibde.gyanmatrix.R;
import com.chibde.gyanmatrix.activity.MainActivity;
import com.chibde.gyanmatrix.activity.PlayerDetailActivity;
import com.chibde.gyanmatrix.model.Favorite;
import com.chibde.gyanmatrix.model.Players;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerHolder> {
    private List<Players> itemList;
    final private List<Players> originList;
    private Context context;

    public PlayerAdapter(Context context, List<Players> itemList) {
        this.itemList = itemList;
        originList = new ArrayList<>();
        originList.addAll(itemList);
        this.context = context;
    }

    @Override
    public PlayerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlayerHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_players, parent, false));
    }

    @Override
    public void onBindViewHolder(final PlayerHolder holder, final int position) {
        final Players player = itemList.get(position);
        holder.name.setText(player.getName());
        holder.matches.setText(String.valueOf(player.getMatches_played() + " Matches"));
        holder.runs.setText(String.valueOf(player.getTotal_score() + " runs"));
        holder.country.setText(player.getCountry());
        PlatformUtil.setImagePicasso(
                context,
                player.getImage(),
                holder.profileImage,
                R.drawable.noimage,
                R.drawable.progress_animation);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) context).startActivityForResult(new Intent(
                        context, PlayerDetailActivity.class)
                        .putExtras(PlatformUtil.bundleSerializable(
                                Constant.GIT_AUTH,
                                player)), Constant.ACTIVITY_RESULT);
            }
        });
        if (player.isFavorite()) {
            holder.favorite.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_yellow_24px));
        } else {
            holder.favorite.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_start_border_white_24px));
        }
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player.isFavorite()) {
                    player.setFavorite(false);
                    originList.get(position).setFavorite(false);
                    notifyItemChanged(position);
                    addFavorite(player, false);
                } else {
                    player.setFavorite(true);
                    originList.get(position).setFavorite(true);
                    addFavorite(player, true);
                    notifyItemChanged(position);
                }
            }
        });
    }

    private void addFavorite(Players players, boolean isFav) {
        if (isFav) {
            Favorite favorite = new Favorite(players.getId(), players.getName());
            SugarRecord.save(favorite);
        } else {
            List<Favorite> favorites = SugarRecord.find(Favorite.class, "player_id = ?", players.getId() + "");
            if (!favorites.isEmpty()) {
                SugarRecord.delete(favorites.get(0));
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void filter(int type) {
        itemList.clear();
        itemList.addAll(originList);
        switch (type) {
            case MainActivity.MATCHES:
                Collections.sort(itemList, new Comparator<Players>() {
                    @Override
                    public int compare(Players o1, Players o2) {
                        return o1.getMatches_played() == o2.getMatches_played() ?
                                0 : (o1.getMatches_played() > o2.getMatches_played() ?
                                -1 : 1);
                    }
                });
                notifyDataSetChanged();
                break;
            case MainActivity.RUNS:
                Collections.sort(itemList, new Comparator<Players>() {
                    @Override
                    public int compare(Players o1, Players o2) {
                        return o1.getTotal_score() == o2.getTotal_score() ?
                                0 : (o1.getTotal_score() > o2.getTotal_score() ?
                                -1 : 1);
                    }
                });
                notifyDataSetChanged();
                break;
            case MainActivity.CLEAR:
                notifyDataSetChanged();
                break;
            case MainActivity.FAVORITE:
                itemList.clear();
                for (Players players : originList) {
                    if (players.isFavorite()) {
                        itemList.add(players);
                    }
                }
                notifyDataSetChanged();
                break;
        }
    }

    //Search items
    public void search(String query) {
        if (query.isEmpty()) {
            itemList.clear();
            itemList.addAll(originList);
            notifyDataSetChanged();
        } else {
            List<Players> playerList = new ArrayList<>();
            for (Players players : originList) {
                if (players.getName().toLowerCase().contains(query.toLowerCase())) {
                    playerList.add(players);
                }
            }
            refreshWithSearchResults(playerList);
        }
    }

    private void refreshWithSearchResults(List<Players> players) {
        applyAndAnimateRemovals(players);
        applyAndAnimateAdditions(players);
        applyAndAnimateMovedItems(players);
    }

    private void applyAndAnimateRemovals(List<Players> newModels) {
        for (int i = itemList.size() - 1; i >= 0; i--) {
            final Players model = itemList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Players> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Players model = newModels.get(i);
            if (!itemList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Players> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Players model = newModels.get(toPosition);
            final int fromPosition = itemList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void notifyFav() {
        List<Favorite> favorites = SugarRecord.listAll(Favorite.class);
        for (int i = 0; i < originList.size(); i++) {
            if (MainActivity.contains(favorites, originList.get(i).getId())) {
                originList.get(i).setFavorite(true);
            } else {
                originList.get(i).setFavorite(false);
            }
        }
        itemList.clear();
        itemList.addAll(originList);
        notifyDataSetChanged();
    }

    private Players removeItem(int position) {
        final Players model = itemList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void addItem(int position, Players model) {
        itemList.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final Players model = itemList.remove(fromPosition);
        itemList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    static class PlayerHolder extends RecyclerView.ViewHolder {
        private TextView name, country, runs, matches;
        private ImageView profileImage;
        private LinearLayout container;
        private ImageButton favorite;

        PlayerHolder(View itemView) {
            super(itemView);
            favorite = (ImageButton) itemView.findViewById(R.id.ib_fav);
            country = (TextView) itemView.findViewById(R.id.tv_country);
            runs = (TextView) itemView.findViewById(R.id.tv_runs);
            matches = (TextView) itemView.findViewById(R.id.tv_matches);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            profileImage = (ImageView) itemView.findViewById(R.id.iv_profile_image);
        }
    }
}
