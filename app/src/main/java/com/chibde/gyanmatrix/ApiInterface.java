package com.chibde.gyanmatrix;

import com.chibde.gyanmatrix.model.Records;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("gyanmatrix")
    Call<Records> getPlayers(
            @Query("type") String json,
            @Query("query") String list_player);
}
