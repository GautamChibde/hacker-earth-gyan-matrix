package com.chibde.gyanmatrix;

import com.gc.comman.App;

public class PlayerApp extends App {

    public static final String API_URL = "http://hackerearth.0x10.info/api/";
    @Override
    public void onCreate() {
        super.onCreate();
        setBaseUrl(API_URL);
        initLocalDb();
    }
}
