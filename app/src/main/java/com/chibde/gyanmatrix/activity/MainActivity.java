package com.chibde.gyanmatrix.activity;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.chibde.gyanmatrix.R;
import com.chibde.gyanmatrix.adapter.PlayerAdapter;
import com.chibde.gyanmatrix.model.Favorite;
import com.chibde.gyanmatrix.model.Players;
import com.chibde.gyanmatrix.model.Records;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.orm.SugarRecord;

import java.util.List;

public class MainActivity
        extends BaseActivity
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private PlayerAdapter playerAdapter;
    public static final int MATCHES = 0, RUNS = 1, CLEAR = 3, FAVORITE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Score Board");
        if (getIntent().getExtras() != null) {
            Records records = (Records) getIntent().getExtras().getSerializable(Constant.RECORDS);
            if (records != null) {
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_players);
                List<Favorite> favorites = SugarRecord.listAll(Favorite.class);
                List<Players> playersList = records.getRecords();
                for (Players players : playersList) {
                    if (contains(favorites, players.getId())) {
                        players.setFavorite(true);
                    } else {
                        players.setFavorite(false);
                    }
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                playerAdapter = new PlayerAdapter(this, playersList);
                recyclerView.setAdapter(playerAdapter);
            }
        }
    }

    public static boolean contains(List<Favorite> favorites, long id) {
        for (Favorite favorite : favorites) {
            if (favorite.getPlayer_id() == id) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        playerAdapter.notifyFav();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playerAdapter.search("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        initSearchView(menu, R.id.app_bar_search);
        return true;
    }

    protected void initSearchView(Menu menu, int viewId) {
        final SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(
                        menu.findItem(viewId)
                );
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_filter:
                PopupMenu popup = new PopupMenu(this, findViewById(R.id.app_filter), Gravity.END);
                popup.inflate(R.menu.option_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.filter_matches:
                                playerAdapter.filter(MATCHES);
                                break;
                            case R.id.filter_runs:
                                playerAdapter.filter(RUNS);
                                break;
                            case R.id.filter_clear:
                                playerAdapter.filter(CLEAR);
                                break;
                            case R.id.filter_fav:
                                playerAdapter.filter(FAVORITE);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        playerAdapter.search(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        playerAdapter.search(newText);
        return false;
    }

    @Override
    public boolean onClose() {
        return false;
    }
}
