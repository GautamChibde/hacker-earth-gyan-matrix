package com.chibde.gyanmatrix.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import com.chibde.gyanmatrix.ApiInterface;
import com.chibde.gyanmatrix.R;
import com.chibde.gyanmatrix.model.Records;
import com.gc.comman.App;
import com.gc.comman.infra.Resource;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

import retrofit2.Call;

public class SplashScreenActivity extends BaseActivity {
    private Call<Records> call;
    private TextView loading_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApiInterface apiService = App.getClient().create(ApiInterface.class);
        call = apiService.getPlayers("json", "list_player");
        loading_text = (TextView) findViewById(R.id.loading_text);
        loading_text.setText(getResources().getString(R.string.loading));
        postServer();
    }

    private void postServer() {
        if (!call.isExecuted()) {
            Resource.enquire(call, Constant.AUTH_REQUEST_CODE);
        } else {
            Resource.enquireClone(call, Constant.AUTH_REQUEST_CODE);
        }
    }

    /**
     * Triggered from {{@link Resource}}
     * @param event Of type{{@link com.gc.comman.infra.Resource.HttpRequestComplete}}
     *              contains response data
     */
    @SuppressWarnings("unused")
    public void onEvent(Resource.HttpRequestComplete event) {
        progress.dismiss();
        if (event.isSuccessful()) {
            if (event.getResponse() != null) {
                if (event.getRequestId() == Constant.AUTH_REQUEST_CODE) {
                    if (event.getResponse() instanceof Records) {
                        Records records = (Records) event.getResponse();
                        startNewActivity(MainActivity.class,
                                PlatformUtil.bundleSerializable(Constant.RECORDS, records));
                        finish();
                    }
                }
            }
        } else {
            loading_text.setText(getResources().getString(R.string.loading_failed));
            networkErrorDialog("There seems to be problem connecting server");
        }
    }

    private void networkErrorDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        postServer();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SplashScreenActivity.this.finish();
                    }
                })
                .setIcon(R.drawable.ic_cricket_bat)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }
}
