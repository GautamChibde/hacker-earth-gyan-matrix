package com.chibde.gyanmatrix.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chibde.gyanmatrix.R;
import com.chibde.gyanmatrix.model.Favorite;
import com.chibde.gyanmatrix.model.Players;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;
import com.orm.SugarRecord;

import java.util.List;

public class PlayerDetailActivity extends BaseActivity {
    private Players players;
    private ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras() != null) {
            players = (Players) getIntent().getExtras().getSerializable(Constant.GIT_AUTH);
            if (players != null) {
                initView();
            }
        }
    }

    private void initView() {
        ImageView profileImage = (ImageView) findViewById(R.id.iv_profile_image);
        TextView details = (TextView) findViewById(R.id.tv_details);
        TextView title = (TextView) findViewById(R.id.tv_title);
        title.setText(players.getName());
        TextView country = (TextView) findViewById(R.id.tv_country);
        TextView runs = (TextView) findViewById(R.id.tv_runs);
        TextView matches = (TextView) findViewById(R.id.tv_matches);
        matches.setText(String.valueOf(players.getMatches_played() + " Matches"));
        runs.setText(String.valueOf(players.getTotal_score() + " Runs"));
        country.setText(players.getCountry());
        PlatformUtil.setImagePicasso(
                this,
                players.getImage(),
                profileImage,
                R.drawable.noimage,
                R.drawable.progress_animation);
        details.setText(players.getDescription());
        imageButton = (ImageButton) findViewById(R.id.ib_fav);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (players.isFavorite()) {
                    players.setFavorite(false);
                    imageButton.setImageDrawable(ContextCompat.getDrawable(
                            PlayerDetailActivity.this,
                            R.drawable.ic_start_border_white_24px));
                    List<Favorite> favorites = SugarRecord.find(
                            Favorite.class,
                            "player_id = ?",
                            players.getId() + "");
                    if (!favorites.isEmpty()) {
                        SugarRecord.delete(favorites.get(0));
                    }
                } else {
                    players.setFavorite(true);
                    imageButton.setImageDrawable(ContextCompat.getDrawable(
                            PlayerDetailActivity.this,
                            R.drawable.ic_star_yellow_24px));
                    Favorite favorite = new Favorite(players.getId(), players.getName());
                    SugarRecord.save(favorite);
                }
            }
        });

        if (isFavorite()) {
            imageButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_yellow_24px));
        } else {
            imageButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_start_border_white_24px));
        }
    }

    public void back(View view) {
        finish();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_player_detail;
    }

    //Check database which player with id is favorite
    public boolean isFavorite() {
        List<Favorite> favoriteList = SugarRecord.find(Favorite.class, "player_id = ?", players.getId() + "");
        return !favoriteList.isEmpty();
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }
}
