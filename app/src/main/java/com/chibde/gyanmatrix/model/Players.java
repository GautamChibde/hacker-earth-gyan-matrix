package com.chibde.gyanmatrix.model;

import java.io.Serializable;

public class Players implements Serializable {
    private long id;
    private String name;
    private String image;
    private long total_score;
    private String description;
    private long matches_played;
    private String country;
    private boolean isFavorite;

    public Players() {
    }

    @Override
    public String toString() {
        return "Players{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", total_score=" + total_score +
                ", description='" + description + '\'' +
                ", matches_played=" + matches_played +
                ", country='" + country + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getTotal_score() {
        return total_score;
    }

    public void setTotal_score(long total_score) {
        this.total_score = total_score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMatches_played() {
        return matches_played;
    }

    public void setMatches_played(long matches_played) {
        this.matches_played = matches_played;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
