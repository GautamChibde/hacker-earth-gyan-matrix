package com.chibde.gyanmatrix.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Records implements Serializable {
    private ArrayList<Players> records;
    private long quote_max;
    private long quote_available;

    @Override
    public String toString() {
        return "Records{" +
                "records=" + records +
                ", quote_max=" + quote_max +
                ", quote_available=" + quote_available +
                '}';
    }

    public ArrayList<Players> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<Players> records) {
        this.records = records;
    }

    public long getQuote_max() {
        return quote_max;
    }

    public void setQuote_max(long quote_max) {
        this.quote_max = quote_max;
    }

    public long getQuote_available() {
        return quote_available;
    }

    public void setQuote_available(long quote_available) {
        this.quote_available = quote_available;
    }
}
