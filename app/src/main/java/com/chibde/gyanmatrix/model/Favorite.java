package com.chibde.gyanmatrix.model;

import com.orm.dsl.Column;
import com.orm.dsl.Table;

@Table(name = "favorite")
public class Favorite {
    private static final Long serialVersionUID = 1L;
    private Long id;
    @Column(name = "player_id")
    private long player_id;

    @Column(name = "name")
    private String name;

    public Favorite() {
    }

    public Favorite(long player_id, String name) {
        this.player_id = player_id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "id=" + id +
                ", player_id=" + player_id +
                ", name='" + name + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(long player_id) {
        this.player_id = player_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
